export class Course {
    constructor(public CourseID: number,
        public Title: String,
        public Credits: number){}
}
